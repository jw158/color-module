from colorama import init, Fore
init()

def print_colored(value, color="white"):
    color_dict = {"white": Fore.WHITE, "black": Fore.BLACK, "red": Fore.RED, "blue": Fore.BLUE, "green": Fore.GREEN, "yellow": Fore.YELLOW}
    print(color_dict[color] + value)