from setuptools import setup, find_packages

setup(
    name="acolorfulmess",
    version="0.0.1",
    description="Delivering colorful characters to a terminal near you (*^▽^*)",
    author="Jonas Wüstner",
    author_email="jw158@hdm-stuttgart.de",
    packages=find_packages(),
    install_requires=["wheel", "colorama"]
)